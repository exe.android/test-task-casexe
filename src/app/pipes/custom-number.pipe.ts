import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'customNumber'
})
export class CustomNumberPipe implements PipeTransform {

    transform(value: number | string): any {
        return new Intl.NumberFormat('ru-RU', {
            minimumFractionDigits: 0
        }).format(Number(value));
    }

}
