import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CommunicateService} from '../services/communicate.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class HeaderComponent implements OnInit {

    constructor(
        private communicateService: CommunicateService
    ) {
    }

    ngOnInit() {
    }

    onRegistrationBtnClicked() {
        this.communicateService.displayRegistrationModal.next(true);
    }

}
