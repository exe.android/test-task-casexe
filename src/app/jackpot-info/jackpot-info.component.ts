import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {JackpotInfo} from '../models/jackpot-info';
import {RefService} from '../services/ref.service';

@Component({
    selector: 'app-jackpot-info',
    templateUrl: './jackpot-info.component.html',
    styleUrls: ['./jackpot-info.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class JackpotInfoComponent implements OnInit {

    public jackpot: JackpotInfo;

    constructor(
        private refService: RefService
    ) {
    }

    ngOnInit() {
        this.refService.getJackpotInfo().subscribe((result: JackpotInfo) => this.jackpot = result);
    }

}
