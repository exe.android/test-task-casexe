import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JackpotInfoComponent } from './jackpot-info.component';

describe('JackpotInfoComponent', () => {
  let component: JackpotInfoComponent;
  let fixture: ComponentFixture<JackpotInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JackpotInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JackpotInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
