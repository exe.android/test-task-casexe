import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-sorter',
    templateUrl: './sorter.component.html',
    styleUrls: ['./sorter.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class SorterComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
