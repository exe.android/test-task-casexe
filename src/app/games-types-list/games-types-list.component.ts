import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {GameType} from '../models/game-type';
import {RefService} from '../services/ref.service';

@Component({
    selector: 'app-games-types-list',
    templateUrl: './games-types-list.component.html',
    styleUrls: ['./games-types-list.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class GamesTypesListComponent implements OnInit {

    public gameTypes: GameType[] = [];

    constructor(
        private refService: RefService
    ) {
    }

    ngOnInit() {
        this.refService.getGameTypes().subscribe((result: GameType[]) => this.gameTypes = result);
    }

}
