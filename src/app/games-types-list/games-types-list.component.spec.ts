import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesTypesListComponent } from './games-types-list.component';

describe('GamesTypesListComponent', () => {
  let component: GamesTypesListComponent;
  let fixture: ComponentFixture<GamesTypesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesTypesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesTypesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
