import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CommunicateService} from '../services/communicate.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class HomeComponent implements OnInit {

    public isDisplayRegistrationModal = false;

    constructor(
        private communicateService: CommunicateService
    ) {
        this.communicateService.displayRegistrationModal.subscribe(value => {
            if (value) {
                this.isDisplayRegistrationModal = true;
                document.getElementsByTagName('body')[0].style.overflow = 'hidden';
            } else {
                this.isDisplayRegistrationModal = false;
                document.getElementsByTagName('body')[0].style.overflow = '';
            }
        });
    }

    ngOnInit() {
    }
}
