export class Promotion {
    public image: string;
    public header: string;
    public description: string;
    public link: string;
}
