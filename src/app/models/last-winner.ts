import {LastWinnersGame} from './last-winners-game';

export class LastWinner {
    public game: LastWinnersGame;
    public winAmount: number;
    public playerName: string;
}
