export class GameListItem {
    public id: number;
    public name: string;
    public image: string;
}
