import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {GameListItem} from '../models/gameListItem';

@Component({
    selector: 'app-games-list-item',
    templateUrl: './games-list-item.component.html',
    styleUrls: ['./games-list-item.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class GamesListItemComponent implements OnInit {

    @Input() game: GameListItem = new GameListItem();

    public isDisplayHover = false;

    constructor() {
    }

    ngOnInit() {
    }

    onBlockMouseEnter() {
        this.isDisplayHover = true;
    }

    onBlockMouseLeave() {
        this.isDisplayHover = false;
    }
}
