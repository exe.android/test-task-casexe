import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {RefService} from '../services/ref.service';
import {GameListItem} from '../models/gameListItem';

@Component({
    selector: 'app-games-list',
    templateUrl: './games-list.component.html',
    styleUrls: ['./games-list.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class GamesListComponent implements OnInit {

    public gamesList: GameListItem[] = [];
    public shortGamesList: GameListItem[] = [];

    constructor(private refService: RefService) {
    }

    ngOnInit() {
        this.refService.getGamesList().subscribe((result: GameListItem[]) => {
            this.shortGamesList = result.slice(0, 6);
            this.gamesList = result.slice(6);
        });
    }

}
