import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-main-slider',
    templateUrl: './main-slider.component.html',
    styleUrls: ['./main-slider.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class MainSliderComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
