import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CommunicateService} from '../services/communicate.service';

@Component({
    selector: 'app-top-nav-bar',
    templateUrl: './top-nav-bar.component.html',
    styleUrls: ['./top-nav-bar.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class TopNavBarComponent implements OnInit {
    public menuItems = ['Все игры', 'Пополнение счета', 'Получить выйгрыш', 'Бонусы', 'Мобильная версия', 'Контакты'];

    constructor(
        private communicateService: CommunicateService
    ) {
    }

    ngOnInit() {
    }

    fastRegistrationLinkClicked() {
        this.communicateService.displayRegistrationModal.next(true);
        return false;
    }
}
