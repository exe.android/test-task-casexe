import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Promotion} from '../models/promotion';
import {NgxCarousel} from 'ngx-carousel';

@Component({
    selector: 'app-promotions',
    templateUrl: './promotions.component.html',
    styleUrls: ['./promotions.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class PromotionsComponent implements OnInit {
    public promotions: Promotion[] = [
        {
            link: '/',
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus tincidunt leo malesuada eros elementum eget eu ligula. `,
            header: 'REACH FOR THE STARS <br/> WITH UP TO 200 FREE SPINS',
            image: '/assets/images/promotions/slide-1.png'
        },
        {
            link: '/',
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus tincidunt leo malesuada eros elementum eget eu ligula. `,
            header: 'REACH FOR THE STARS <br/> WITH UP TO 200 FREE SPINS',
            image: '/assets/images/promotions/slide-1.png'
        },
        {
            link: '/',
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus tincidunt leo malesuada eros elementum eget eu ligula. `,
            header: 'REACH FOR THE STARS <br/> WITH UP TO 200 FREE SPINS',
            image: '/assets/images/promotions/slide-1.png'
        },
        {
            link: '/',
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus tincidunt leo malesuada eros elementum eget eu ligula. `,
            header: 'REACH FOR THE STARS <br/> WITH UP TO 200 FREE SPINS',
            image: '/assets/images/promotions/slide-1.png'
        },
    ];

    public slider: NgxCarousel;

    constructor() {
    }

    ngOnInit() {
        this.slider = {
            grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
            slide: 1,
            speed: 400,
            interval: 4000,
            animation: 'lazy',
            point: {
                visible: true,
                pointStyles: `
                    .ngxcarouselPoint {
                        list-style-type: none;
                        padding: 12px;
                        margin: 0;
                        white-space: nowrap;
                        overflow: auto;
                        position: absolute;
                        width: 100%;
                        top: 0;
                        left: 0;
                        box-sizing: border-box;
                    }
                    .ngxcarouselPoint li {
                        display: inline-block;
                        border-radius: 999px;
                        background: #babab5;
                        box-shadow: 1px 3px 5px #000000ad;
                        padding: 5px;
                        margin: 0 3px;
                        transition: .4s ease all;
                    }
                    .ngxcarouselPoint li.active {
                        box-shadow: 0 0 16px 6px #e0d6ff, inset 0px -1px 1px 1px #a69dbb;
                        border: 1px solid #9487b5bd;
                        margin: -3px 3px;
                        padding: 6px;
                    }
            `
            },
            load: 2,
            touch: true,
            loop: true
        };
    }

}
