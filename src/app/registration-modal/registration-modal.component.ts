import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {CommunicateService} from '../services/communicate.service';
import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';

@Component({
    selector: 'app-registration-modal',
    templateUrl: './registration-modal.component.html',
    styleUrls: ['./registration-modal.component.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    animations: [
        trigger('flyInOut', [
            state('in', style({transform: 'translateX(0)'})),
            transition('void => *', [
                style({transform: 'scale(1.6)'}),
                animate(300)
            ]),
            transition('* => void', [
                animate(100, style({transform: 'scale(1.6)'}))
            ])
        ])
    ]
})
export class RegistrationModalComponent implements OnInit, OnChanges {

    public registrationForm: FormGroup;
    public ipAddressesList: string[] = [];
    public isIpAddressInputFocused = false;
    public ipAddressesLimit = 5;
    public containerVisible = false;

    @Input() visible = false;


    constructor(private fb: FormBuilder, private communicateService: CommunicateService) {
        this.registrationForm = fb.group({
            'name': ['', Validators.compose([Validators.required, Validators.pattern(/^[A-zА-я]+$/i)])],
            'email': ['', Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)])],
            'ip': ['', Validators.pattern(/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/i)],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(16)])],
            'confirmPassword': ['', Validators.required]
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.hasOwnProperty('visible')) {
            if (changes.visible.currentValue) {
                this.containerVisible = true;
            }

            if (!changes.visible.currentValue) {
                this.registrationForm.reset();
                this.ipAddressesList = [];
            }
        }
    }

    passwordsAreMatches(): boolean {
        return this.registrationForm.get('password').value === this.registrationForm.get('confirmPassword').value;
    }

    ngOnInit() {
    }

    onAddIpButtonClicked() {
        const ipControl = this.registrationForm.get('ip');
        const ip = ipControl.value;

        if (!ip || ipControl.invalid) {
            return false;
        }

        if (this.ipAddressesList.indexOf(ip) !== -1) {
            ipControl.setErrors({
                'alreadyInList': true
            });

            return false;
        }

        if (this.ipAddressesList.length === this.ipAddressesLimit) {
            ipControl.setErrors({
                'limitReached': true
            });

            return false;
        }

        this.ipAddressesList.push(ip);
        ipControl.reset();
        return false;
    }

    onRemoveIpButtonClicked(ip) {
        this.ipAddressesList.splice(this.ipAddressesList.indexOf(ip), 1);
        const ipControl = this.registrationForm.get('ip');
        if (ipControl.getError('limitReached')) {
            ipControl.setErrors({
                'limitReached': false
            });

            ipControl.updateValueAndValidity();
        }

        return false;
    }

    onIpInputKeyUp(event: KeyboardEvent) {
        if (event.key === 'Enter') {
            this.onAddIpButtonClicked();
        }
    }

    onRegistrationFormSubmit() {
        if (this.isIpAddressInputFocused && this.registrationForm.get('ip').value && this.registrationForm.get('ip').value.length) {
            console.log('IP input focused. Not submitting');
        } else if (this.registrationForm.valid) {
            const request = {
                ipAddresses: this.ipAddressesList,
                ...this.registrationForm.value
            };

            console.log('Everything is okay. Submitting form');
        }

        return false;
    }

    onCloseModalLinkClicked() {
        this.communicateService.displayRegistrationModal.next(false);
    }
}
