import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastWinnersInfoComponent } from './last-winners-info.component';

describe('LastWinnersInfoComponent', () => {
  let component: LastWinnersInfoComponent;
  let fixture: ComponentFixture<LastWinnersInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastWinnersInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastWinnersInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
