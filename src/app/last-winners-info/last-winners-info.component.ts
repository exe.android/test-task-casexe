import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgxCarousel} from 'ngx-carousel';
import {LastWinner} from '../models/last-winner';
import {RefService} from '../services/ref.service';

@Component({
    selector: 'app-last-winners-info',
    templateUrl: './last-winners-info.component.html',
    styleUrls: ['./last-winners-info.component.scss']
})
export class LastWinnersInfoComponent implements OnInit {

    public carousel: NgxCarousel;
    public lastWinners: LastWinner[] = [];

    constructor(private refService: RefService) {
    }

    ngOnInit() {
        this.carousel = {
            grid: {xs: 3, sm: 3, md: 3, lg: 3, all: 0},
            slide: 1,
            speed: 400,
            interval: 4000,
            animation: 'lazy',
            point: {
                visible: false
            },
            load: 2,
            touch: true,
            loop: true
        };

        this.refService.getLastWinners().subscribe((result: LastWinner[]) => this.lastWinners = result);
    }
}
