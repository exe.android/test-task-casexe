import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {routes} from './app.routing';
import { NgxCarouselModule } from 'ngx-carousel';
import 'hammerjs';

import {AppComponent} from './app.component';
import {TopNavBarComponent} from './top-nav-bar/top-nav-bar.component';
import {HeaderComponent} from './header/header.component';
import {MainSliderComponent} from './main-slider/main-slider.component';
import {JackpotInfoComponent} from './jackpot-info/jackpot-info.component';
import {LastWinnersInfoComponent} from './last-winners-info/last-winners-info.component';
import {GamesTypesListComponent} from './games-types-list/games-types-list.component';
import {SearchComponent} from './search/search.component';
import {SorterComponent} from './sorter/sorter.component';
import {PromotionsComponent} from './promotions/promotions.component';
import {GamesListComponent} from './games-list/games-list.component';
import {FooterComponent} from './footer/footer.component';
import {HomeComponent} from './home/home.component';
import {RegistrationModalComponent} from './registration-modal/registration-modal.component';
import {RouterModule} from '@angular/router';
import { ButtonComponent } from './button/button.component';
import {RefService} from './services/ref.service';
import { CustomNumberPipe } from './pipes/custom-number.pipe';
import { GamesListItemComponent } from './games-list-item/games-list-item.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommunicateService} from './services/communicate.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        AppComponent,
        TopNavBarComponent,
        HeaderComponent,
        MainSliderComponent,
        JackpotInfoComponent,
        LastWinnersInfoComponent,
        GamesTypesListComponent,
        SearchComponent,
        SorterComponent,
        PromotionsComponent,
        GamesListComponent,
        FooterComponent,
        HomeComponent,
        RegistrationModalComponent,
        ButtonComponent,
        CustomNumberPipe,
        GamesListItemComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        NgxCarouselModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
    ],
    providers: [RefService, FormBuilder, CommunicateService],
    bootstrap: [AppComponent],
})
export class AppModule {
}
