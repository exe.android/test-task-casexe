import {Injectable} from '@angular/core';
import {LastWinner} from '../models/last-winner';
import {LastWinnersGame} from '../models/last-winners-game';
import {JackpotInfo} from '../models/jackpot-info';
import {Observable} from 'rxjs/Observable';
import {GameType} from '../models/game-type';
import {GameListItem} from '../models/gameListItem';

@Injectable()
export class RefService {

    private lastWinnersGames: LastWinnersGame[] = [
        {
            gameImage: '/assets/images/games/last-winners/games-of-thrones.png',
            gameName: 'Games of Thrones'
        },
        {
            gameImage: '/assets/images/games/last-winners/ghotic.png',
            gameName: 'Ghotic'
        },
        {
            gameImage: '/assets/images/games/last-winners/robyn.png',
            gameName: 'Robyn'
        }
    ];

    constructor() {
    }

    // Angular 5 возвращает observable при http запросах.
    // Эмулируем http запрос на стадии отсутствия API, чтобы клиентский код не пришлось изменять.
    public getLastWinners(): Observable<LastWinner[]> {
        return new Observable((observer => {
            const winners: LastWinner[] = [
                {
                    game: this.lastWinnersGames[1],
                    playerName: 'Insight',
                    winAmount: 150
                },
                {
                    game: this.lastWinnersGames[0],
                    playerName: 'Some player',
                    winAmount: 200
                },
                {
                    game: this.lastWinnersGames[2],
                    playerName: 'Other player',
                    winAmount: 210
                },
                {
                    game: this.lastWinnersGames[0],
                    playerName: 'Other player 2',
                    winAmount: 130
                },
                {
                    game: this.lastWinnersGames[2],
                    playerName: 'Other player 3',
                    winAmount: 145
                },
                {
                    game: this.lastWinnersGames[1],
                    playerName: 'Other player 4',
                    winAmount: 320
                },
                {
                    game: this.lastWinnersGames[1],
                    playerName: 'Other player 5',
                    winAmount: 360
                },
                {
                    game: this.lastWinnersGames[2],
                    playerName: 'Other player 6',
                    winAmount: 120
                }
            ];

            observer.next(winners);
            observer.complete();
        }));
    }

    public getJackpotInfo(): Observable<JackpotInfo> {
        return new Observable((observer) => {
            const jackpot = new JackpotInfo();
            jackpot.gold = 7859352;
            jackpot.silver = 2148759;
            jackpot.bronze = 1589654;

            observer.next(jackpot);
            observer.complete();
        });
    }

    public getGameTypes(): Observable<GameType[]> {
        return new Observable((observer) => {
            const gameTypes: GameType[] = [
                {
                    icon: '/assets/images/game-types/popular.png',
                    name: 'Popular'
                },
                {
                    icon: '/assets/images/game-types/new.png',
                    name: 'New'
                },
                {
                    icon: '/assets/images/game-types/slot.png',
                    name: 'Slot games'
                },
                {
                    icon: '/assets/images/game-types/cards.png',
                    name: 'Card games'
                },
                {
                    icon: '/assets/images/game-types/roulette.png',
                    name: 'Roulette'
                }
            ];

            observer.next(gameTypes);
            observer.complete();
        });
    }

    public getGamesList(): Observable<GameListItem[]> {
        return new Observable((observer) => {
            const games: GameListItem[] = [
                {
                    id: 1,
                    image: '/assets/images/games/games-list/anubis-secret.png',
                    name: 'Anubis\' secret'
                },
                {
                    id: 2,
                    image: '/assets/images/games/games-list/dream-of-knight.png',
                    name: 'Dream of knight'
                },
                {
                    id: 3,
                    image: '/assets/images/games/games-list/lucky-magic.png',
                    name: 'Lucky magic'
                },
                {
                    id: 4,
                    image: '/assets/images/games/games-list/money-scent.png',
                    name: 'Money scent'
                },
                {
                    id: 5,
                    image: '/assets/images/games/games-list/submarine.png',
                    name: 'Submarine'
                }
            ];

            const gamesList: GameListItem[] = [];

            for (let i = 0; i < 21; i++) {
                const gameIndex = Math.round(Math.random() * (games.length - 1));
                gamesList.push(games[gameIndex]);
            }

            observer.next(gamesList);
            observer.complete();
        });
    }
}
