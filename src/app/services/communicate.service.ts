import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class CommunicateService {

    public displayRegistrationModal: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() {
    }

}
